<?php
    
    class Controller{
        
        function __construct() {
            Session::init();
            $this->loadModel();
        }
        
        function loadModel(){
            $model = get_class($this).'_model';
            $path = 'models/'.$model.'.php';
            if(file_exists($path)){
                
                require $path;
                $this->model = new $model();
                
            }    
        }
        
        function initMethods(){
            
            switch ($this->method) {
                case "put":
                    $_PUT = $this->phpinput;
                    parse_str($_PUT, $_PUT);
                    return $_PUT;
                    break;
                case "delete":
                    $_DELETE = $this->phpinput;
                    parse_str($_DELETE, $_DELETE);
                    return $_DELETE;
                    break;
            }
        
        }
        
    }
?>