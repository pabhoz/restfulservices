<?php

class Hasher extends Controller {

    function __construct() {
        parent::__construct();
    }

    function get() {
        if (isset($_GET["data"])) {
            print(Hash::create(HASH_ALGO, $_GET["data"], HASH_KEY));
        }
    }

}
