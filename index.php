<?php
	
	require './config.php';
	// --> Controller/Params
	$url = (isset($_GET["url"])) ?  $_GET["url"] : "Index" ;
	
	$url = explode("/",$url);

	if(isset($url[0])){ $controller = $url[0]; }
	if(isset($url[1])){ $params = $url[1]; }
        
	spl_autoload_register(function ($class) {
		if(file_exists("./libs/" . $class .".php")){
			require "./libs/" . $class .".php";
		}
	});
        
        if(isset($_GET["key"])&&isset($_GET["email"])){
            require './controllers/Usuario.php';
            $userCtrlr = new Usuario();
            if(!$userCtrlr->validateKey($_GET["key"], $_GET["email"])){
               echo "Su API No es válida";
             return false; 
            }
        }else{
             echo "No se ha determinado una API Key";
             return false;
        }
        
        $request = new Request();

        if(file_exists('./controllers/'.$controller.'.php')){
            require './controllers/'.$controller.'.php';
            $controller = new $controller();

            foreach ($request as $key => $value) {
                $controller->$key = $value;
            }

                if(method_exists($controller, $request->method)){
                    if(isset($params)){
                            $controller->{$request->method}($params);
                    }else{
                            $controller->{$request->method}();
                    }
                }else{
                    print("No existen acciones para esa petición");
                }
            
        }else{
            echo 'Not Found';
        }

?>
